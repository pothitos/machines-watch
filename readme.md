# Machines Watch

✔️ Collects Linux health metrics for a given list of hosts every day

✔️ Stores them in MongoDB

✔️ Presents them via Mongo Express front-end

## Limitation

 - The username for the hosts is `ubuntu`

## Execution

 - Clone the project

 - Inside its folder create a `host-keys` folder containing

    * all the hosts that you want to monitor as file names

    * the corresponding SSH keys as file contents

 - If you have Docker Compose installed, execute
   ```
   docker compose up -d
   ```

 - Navigate with your browser to `http://localhost:8081`
