#!/bin/sh

: "${REFRESH_INTERVAL:=1d}"

mkdir -p ~/.ssh
echo "Host *
        StrictHostKeyChecking no" >> ~/.ssh/config

echo "Wait for MongoDB initialization"
until mongoexport --host mongo --db Machines --collection Watch > /dev/null
do
  sleep 1
done

cd /data/hosts-keys || exit 1

while true
do
  for HOST_KEY in *
  do
    USERS=$(ssh -i "$HOST_KEY" "ubuntu@$HOST_KEY" uptime | grep -o "[0-9]* user" | grep -o "[0-9]*")
    CPU=$(ssh -i "$HOST_KEY" "ubuntu@$HOST_KEY" uptime | grep -o "[0-9]*\\.[0-9]*$")
    CPU=$(echo "($CPU * 100) / 1" | bc)
    # Division by 1 truncates the decimal portion
    MEMORY=$(ssh -i "$HOST_KEY" "ubuntu@$HOST_KEY" free | awk '/Mem:/ {print int(100 * $3 / $2)}')
    DISK=$(ssh -i "$HOST_KEY" "ubuntu@$HOST_KEY" df / | grep -o "[0-9]\\+%")
    echo "{\"date\": \"$(date +%Y-%m-%d)\", \"host\": \"$HOST_KEY\", \"users\": $USERS,
           \"cpu\": \"$CPU%\", \"memory\": \"$MEMORY%\", \"disk\": \"$DISK\"}" | \
         mongoimport --host mongo --db Machines --collection Watch
  done
  echo "Sleep until new run"
  sleep "$REFRESH_INTERVAL"
done
